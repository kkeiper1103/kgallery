var kGallery = function (oDataSource, oOptions) {

	/* ------------------------------ */

	// @access public
	this.dataSource = null;
	this.options = null;
	this.rootEl = null;
	this.selImageContainer = null;
	this.inTransition = false;
	this.transitions = null;

	/* ------------------------------ */

	// handle for object reference inside private functions.
	// @access private 
	var kGallery = this;

	/* ------------------------------ */

	// Transitions Object
	// @access public
	// @return 0 < Number < 1
	
	this.transitions = {
		'easeIn': function (t, d) {
			return Math.pow(t / d, Math.cos(1));
		},
			'easeOut': function (t, d) {
			return (1 - Math.pow(1 - (t / d), Math.sin(1)))
		},
			'easeInStrong': function (t, d) {
			return (Math.pow(t / d, 5))
		},
			'easeOutStrong': function (t, d) {
			return (1 - Math.pow(1 - (t / d), 5))
		},
			'easeInOutSine': function(t, d){
			return (-1/2 * (Math.cos(Math.PI*t/d) - 1) + 0);
		}
	};

	/* ------------------------------ */

	// Utilities Object for getting elements, creating, and modifying them.
	// @access private
	var util = {

		byId: function (id) {
			return document.getElementById(id)
		},
		byClass: function (className) {
			return document.getElementsByClassName(className)
		},
		bySelector: function (sel) {
			if (document.querySelectorAll) {
				return document.querySelectorAll(sel)
			} else {
				var el = new Array();

				switch (sel.substr(0, 1)) {
					case "#":
						el.push(document.getElementById(sel.substr(1)));
						break;
					case ".":
						el = document.getElementsByClassName(sel.substr(1));
						break;
					default:
						el = document.getElementsByTagName(sel.substr(1));
						break;
				}

				return el;
			}
		},

		log: function (msg) {
			if (window.console) {
				console.log(msg);
			}
		},

		create: function (el) {
			return document.createElement(el);
		},

		addClass: function (el, cls) {
			if (!this.hasClass(el, cls)) {
				el.className += " " + cls;
				el.className = el.className.replace(/^\s+|\s+$/g, '');
			}
		},
		hasClass: function (el, cls) {
			return el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
		},
		removeClass: function (el, cls) {
			if (this.hasClass(el, cls)) {
				var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
				el.className = el.className.replace(reg, ' ');
				el.className = el.className.replace(/^\s+|\s+$/g, '');
			}
		}

	};

	/* ------------------------------ */

	var preload_images = function () {

		var images = new Array();
		var length = kGallery.dataSource.length;
		var count = 0;

		var overlay = util.create("div");

		overlay.setAttribute("style", "position:absolute;left:0;top:0;width:100%;height:100%;background-color: rgba(0, 0, 0, 0.8);z-index:99;");

		var res = util.byId(kGallery.options.sElement.substr(1)).appendChild(overlay);

		util.log(res);

		for (index in kGallery.dataSource) {
			var img = new Image();
			img.src = kGallery.dataSource[index].src;
			img.onload = function () {
				count++;
				if (count == length) {
				
					// remove overlay code goes here...
					util.byId(kGallery.options.sElement.substr(1)).removeChild(overlay);
					util.log("loaded"); // All Loaded
				}
			}

			images.push(img);
		}
		
		
	}

	/* ------------------------------ */

	var set_data_source = function (oDataSource) {
		kGallery.dataSource = oDataSource;
	}

	/* ------------------------------ */

	var set_options = function (oOptions) {

		kGallery.options = {
			tWidth: "50px",
			tHeight: "50px",

			iWrapper: 'div',

			inTransition: 'easeIn',
			outTransition: 'easeOut',
			transitionTime: '500',

			debug: false
		};

		for (prop in oOptions)
		kGallery.options[prop] = oOptions[prop];

	}

	/* ------------------------------- */

	var init = function () {
		kGallery.rootEl = document.querySelector(kGallery.options.sElement);
		kGallery.rootEl.style.width = kGallery.options.iWidth;

		// add kgal class to root element;

		util.addClass(kGallery.rootEl, "kgallery");

		if (kGallery.options.debug == true) {
			util.log(kGallery);
		}
		
		var curImgs = util.bySelector("img");
		
		var images = new Array();
		for(var img = 0; img < curImgs.length; img++){
			images.push({
				src: curImgs[img].src,
				alt: curImgs[img].alt || "",
				id: curImgs[img].id || "",
				title: curImgs[img].title || ""
			});
			//util.log( curImgs[img] );
		}
		
		for(var i=0;i<images.length;i++){
			kGallery.dataSource.push(images[i]);
		}
		
		// remove all things inside the viewer
		
		while(kGallery.rootEl.firstChild){
			kGallery.rootEl.removeChild( kGallery.rootEl.firstChild );
		}

	}

	/* ------------------------------- */

	var create_dom = function () {

		
	  
	  // Append stylesheet to head section
	  
	  var ss = document.createElement( "link" );
	  ss.setAttribute('href', 'css/kgallery.css?q=' + (new Date()).getTime() ); // append date timestamp so that the css file is force loaded
	  ss.setAttribute('media', 'all');
	  ss.setAttribute('rel', 'stylesheet');
	  
	  document.head.insertBefore(ss, document.head.firstElementChild.nextElementSibling );
	
		// Create Thumbnail List
		
		var ul_container = util.create("div");
		ul_container.setAttribute('id','thumbnail_container');
		ul_container.style.width = parseInt(kGallery.options.iWidth) + "px";
		ul_container.style.left = '0px';
		ul_container.style.overflow = 'hidden';
		ul_container.style.position = 'relative';
		
		var width = parseInt(kGallery.options.iWidth);
		
		if( width < 480 )
		{
			//include 360px size controls
			var ss = document.createElement( "link" );
			  ss.setAttribute('href', 'css/kgallery-controls-360.css?q=' + (new Date()).getTime() );
			  ss.setAttribute('media', 'all');
			  ss.setAttribute('rel', 'stylesheet');
			  
			  document.head.insertBefore(ss, document.head.firstElementChild.nextElementSibling );
		}
		else if( width >= 480 && width < 600 )
		{
			//include 480px size controls
			var ss = document.createElement( "link" );
			  ss.setAttribute('href', 'css/kgallery-controls-480.css?q=' + (new Date()).getTime() );
			  ss.setAttribute('media', 'all');
			  ss.setAttribute('rel', 'stylesheet');
			  
			  document.head.insertBefore(ss, document.head.firstElementChild.nextElementSibling );
		} else 
		{
			// include 600px size controls
			var ss = document.createElement( "link" );
			  ss.setAttribute('href', 'css/kgallery-controls-600.css?q=' + (new Date()).getTime() );
			  ss.setAttribute('media', 'all');
			  ss.setAttribute('rel', 'stylesheet');
			  
			  document.head.insertBefore(ss, document.head.firstElementChild.nextElementSibling );
			
		}

		var ul = document.createElement("ul");
		util.addClass(ul, 'tn');
		
		ul.setAttribute("id",'tn_list');
		ul.style.whiteSpace = "nowrap";
		ul.style.marginLeft = "0";
		
		
		ul_container.appendChild( ul );
		
		
		var i=0; // counter to figure first image. more elegant way to do it??
		for (index in kGallery.dataSource) {
			var li = document.createElement("li");

			var img = document.createElement("img");

			for (prop in kGallery.dataSource[index]) {
				img.setAttribute(prop, kGallery.dataSource[index][prop]);
			}

			util.addClass(img, "thumb");

			img.style.width = kGallery.options.tWidth;
			img.style.height = kGallery.options.tHeight;
			
			//select first image
			if(i===0){util.addClass(img,'selected');}

			// onclick function
			img.onclick = function (e) {


				var evtElement = null;

				if (e.srcElement) { // Chrome

					evtElement = e.srcElement;

				} else if (e.currentTarget) { // Firefox

					evtElement = e.currentTarget;

				}


				if (evtElement.src == kGallery.selImageContainer.firstElementChild.src || kGallery.inTransition == true) return;



				kGallery.inTransition = true;

				// outTransition
				var transOut = function () {
					var i = 0;

					var interval = setInterval(function () {

						var factor = kGallery.transitions[kGallery.options.outTransition](i, kGallery.options.transitionTime); // in milliseconds

						kGallery.selImageContainer.firstElementChild.style.opacity = (1 - factor);

						i += 10;

						var opacity = parseFloat(kGallery.selImageContainer.firstElementChild.style.opacity);

						if ((1 - factor) == 0 ||
							i >= kGallery.options.transitionTime) {
							clearInterval(interval);

							var selected = util.byClass("selected");

							if (selected.length > 0) {
								util.removeClass(selected[0], 'selected');
							}

							util.addClass(evtElement, "selected");

							kGallery.selImageContainer.firstElementChild.src = evtElement.src;

							// possible do height transition?
							heightTrans();

							//transIn();

						}

					}, 10);
				}
				
				// heightTrans
				var heightTrans = function(){
					var i = 0;
					var interval = setInterval(function(){
						
						var selImageContainer = util.byId('image_container');
						
						var curHeight = Math.ceil(parseInt(selImageContainer.offsetHeight));
						
						var scale = (parseInt(kGallery.options.iWidth) / evtElement.naturalWidth); // ratio of displayed width to natural width; needed to find height;
						
						var endHeight = Math.ceil(scale * evtElement.naturalHeight) - curHeight;
						
						var factor = kGallery.transitions.easeInOutSine(/*startTime*/ i, /*finishTime*/ kGallery.options.transitionTime);
						
						//util.log( endHeight * factor );
						
						
						
						selImageContainer.style.height = curHeight + (endHeight * factor) + "px";
						
						i += 10;
						if( parseInt(curHeight) == Math.ceil(scale * evtElement.naturalHeight) ||
							i >= kGallery.options.transitionTime){
							
							clearInterval(interval);
							
							transIn();
						}
						
					}, 10);
				}
				
				// inTransition
				var transIn = function () {
					var i = 0;

					var interval = setInterval(function () {

						var factor = kGallery.transitions[kGallery.options.inTransition](i, kGallery.options.transitionTime); // in milliseconds

						kGallery.selImageContainer.firstElementChild.style.opacity = factor;

						i += 10;

						var opacity = parseFloat(kGallery.selImageContainer.firstElementChild.style.opacity);

						if (opacity == 1  ||
							i >= kGallery.options.transitionTime) {
							clearInterval(interval);

							kGallery.inTransition = false;
						}

					}, 10);
				}

				transOut();
				
				var caption = util.byId("caption");
				caption.innerHTML = evtElement.alt;

			}

			li.appendChild(img);

			ul.appendChild(li);
			i++;
		}

		kGallery.rootEl.appendChild(ul_container);

		// Create Previewer

		var image_container = document.createElement('div');
		image_container.setAttribute('id', 'image_container');

		kGallery.selImageContainer = image_container;

		kGallery.rootEl.appendChild(image_container);

		// Append First Image

		var selected_image = document.createElement('img');
		for (prop in kGallery.dataSource[0])
		selected_image[prop] = kGallery.dataSource[0][prop];

		selected_image.setAttribute('id', 'selectedImage');
		selected_image.style.width = kGallery.options.iWidth;

		image_container.appendChild(selected_image);
		
		// Create Caption Area
		
		var caption = util.create("div");
		
		var caption_text = util.create("p");
		caption_text.setAttribute('id','caption');
		caption_text.innerHTML = selected_image.alt;
		
		caption.setAttribute('id','caption_container');
		caption.style.width = kGallery.options.iWidth;
		
		caption.appendChild( caption_text );
		
		kGallery.rootEl.appendChild( caption );
		
		// Create Controls
		
		var controls = function(){
			
			var controls_container = util.create("div");
			controls_container.setAttribute('id','controls_container');
			
			var play = util.create("div"),
				stop = util.create("div"),
				back = util.create("div"),
				next = util.create("div"),
				first = util.create("div"),
				last = util.create("div");
				
			
			play.id = "play";
			stop.id = 'stop';
			back.id = 'back';
			next.id = 'next';
			first.id = 'first';
			last.id = 'last';
			
			
			
			var click_handler = function(e){
				
				if(e.preventDefault){
					e.preventDefault();
				}
				
				if( kGallery.inTransition )
					return false;
				
				
				
				var evtElement = null;

				if (e.srcElement) { // Chrome

					evtElement = e.srcElement;

				} else if (e.currentTarget) { // Firefox

					evtElement = e.currentTarget;

				}
				
				var dist = 0;
				
				switch( evtElement['id'] ){
					case "next": dist = -200;break;
					case "back": dist = 200; break;
					default: break;
				}
				
				var tn_list = util.byId("tn_list");
				
				var left = parseInt(tn_list.style.marginLeft);
				var right = parseInt(tn_list.offsetWidth + tn_list.offsetLeft);
				
				
				if( (left + dist) > 0 ){
					return false;
				}
				
				// Do animation here
				var i = 0;
				kGallery.inTransition = true;
				var interval = setInterval(function(){
					
					var factor = kGallery.transitions.easeInOutSine(i, 500); // in milliseconds

						
						
						tn_list.style.marginLeft = left + (factor * dist) + "px";

						i += 10;
					
					// if the property's current  value is at the end value
					if(parseInt(tn_list.style.marginLeft) == left + dist){
						
						// stop the interval loop.
						clearInterval(interval);
						
						// run any "callback" functions here, ie functions that can't be run until
						// the animation is complete.
						
						// nextAnim();
						
						kGallery.inTransition = false;
					}
					
				}, 10);
				
				// End Animation
				
				
				return false;
			};
			
			back.onclick = click_handler;
			next.onclick = click_handler;
			
			controls_container.appendChild(play);
			controls_container.appendChild(stop);
			controls_container.appendChild(back);
			controls_container.appendChild(next);
			controls_container.appendChild(first);
			controls_container.appendChild(last);
			
			var controls = controls_container.getElementsByTagName("div");
			
			for(var i=0; i<controls.length;i++){
				util.addClass( controls[i], 'off' );
			}
			
			kGallery.rootEl.insertBefore(controls_container, util.byId('image_container') );
			
		}
		controls();

	}

	/* ------------------------------- */

	set_data_source(oDataSource);
	set_options(oOptions);

	init();

	preload_images();

	create_dom();

}
